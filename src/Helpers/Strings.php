<?php

namespace Shantilab\BxTools\Helpers;

/**
 * Class Strings
 * @package Shantilab\BxTools\Helpers
 */
class Strings
{
    /**
     * @param $n
     * @param $forms
     * @return mixed
     */
    public static function pluralForm($n, $forms)
    {
        return $n%10==1&&$n%100!=11?$forms[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$forms[1]:$forms[2]);
    }

    /**
     * @param $phone
     * @return bool
     */
    public static function isPhone($phone)
    {
        if (preg_match('/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/iu', $phone))
            return true;

        return false;
    }
}