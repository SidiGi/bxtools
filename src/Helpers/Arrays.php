<?php
namespace Shantilab\BxTools\Helpers;

/**
 * Class Arrays
 * @package Shantilab\BxTools\Helpers
 */
class Arrays
{
    /**
     * @param $sortArray
     * @return \Closure
     */
    public static function sortItemsByIds($sortArray) {
        return function ($a, $b) use ($sortArray) {
            $first = array_search($a['ID'], $sortArray);
            $second = array_search($b['ID'], $sortArray);
            if ($first == $second) {
                return 0;
            }
            return ($first < $second) ? -1 : 1;
        };
    }
}