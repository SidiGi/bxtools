<?php
namespace Shantilab\BxTools;

use Bitrix\Main\Config\Configuration,
    Bitrix\Main\Application,
    Bitrix\Main\IO,
    Symfony\Component\Yaml\Yaml;

class BxToolsConfig
{
    const BX_TOOLS_CONFIG = [
        'partials' => [
            'delimiter' =>  '.',
            'path' => 'partials.root_path',
            'component_parameters' => 'partials.component_parameters_path',
        ],
        'settings' => [
            'delimiter' =>  '.',
            'path' => '/local/php_interface/settings.yml',
            'iblocks' => 'app.data.iblocks',
        ]
    ];
    const BX_CONFIG_KEY = 'bxtools';
    protected $bitrixConfig;

    /**
     * BxToolsAdapter constructor.
     */
    public function __construct()
    {
        $this->bitrixConfig = Configuration::getInstance();
    }


    public function get(){
        if (class_exists('\Bitrix\Main\Config\Configuration'))
        {
            $config = $this->bitrixConfig->get(self::BX_CONFIG_KEY);
            if (is_array($config) && !empty($config))
            {
                return $config;
            }

            return null;
        }
    }

    public function install(){
        $this->bitrixConfig->add(self::BX_CONFIG_KEY, self::BX_TOOLS_CONFIG);
        $this->bitrixConfig->saveConfiguration();

        $this->saveSettings(self::BX_TOOLS_CONFIG);
    }

    protected function saveSettings(array $bxConfigData = []){
        $file = new IO\File(
            Application::getDocumentRoot() . $bxConfigData['settings']['path']
        );
        if (!$file->isExists()){
            file_put_contents($file->getPath(), Yaml::dump([
                'partials'=> [
                    'root_path' => '/local/php_interface/partials/',
                    'component_parameters_path' => '/local/php_interface/component_partials/'
                ],
                'app' => [
                    'data' => [
                        'iblocks' => ''
                    ]
                ]
            ]));
        }
    }
}
