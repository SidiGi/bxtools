<?php
namespace Shantilab\BxTools;

use Bitrix\Main\Application,
    Bitrix\Main\Web\Uri;

/**
 * Class Page
 * @package Shantilab\BxTools
 */
class Page
{
    public $uri;

    /**
     * Page constructor.
     * @param null $uri
     */
    public function __construct($uri = null)
    {
        if (!$uri){
            $request = Application::getInstance()->getContext()->getRequest();
            $uri = $request->getRequestUri();
        }

        $this->uri = new Uri($uri);
    }

    /**
     * @param $path
     * @return bool
     */
    public function is($path){
        if (!$path){
            return false;
        }

        if (is_array($path)){
            foreach($path as $item){
                if($this->is($item)){
                    return true;
                }
            }
        }

        if (!strpos('.php', $path))
            $path .= 'index.php';

        $path = str_replace('//', '/', SITE_DIR . $path);

        if (\CSite::InDir($path))
            return true;

        return false;
    }

    /**
     * @param $path
     * @return bool
     */
    public function in($path){
        if (!$path){
            return false;
        }

        if (is_array($path)){
            foreach($path as $item){
                if($this->in($item))
                    return true;
            }
        }

        $path = str_replace('//', '/', SITE_DIR . $path);

        if (\CSite::InDir($path))
            return true;

        return false;
    }

    /**
     * @return bool
     */
    public function isIndex(){
        return $this->is('/');
    }

    /**
     * @return bool
     */
    public function isAjax(){
        return ! (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            || empty($_SERVER['HTTP_X_REQUESTED_WITH'])
            || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest');
    }

    /**
     * @return bool
     */
    public function is404(){
        return defined('ERROR_404');
    }
}