<?php
namespace Shantilab\BxTools\Partials;

use Bitrix\Main\Application;
use Shantilab\BxTools\Config;

/**
 * Class Component
 * @package Shantilab\BxTools
 */
class Component
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * Component constructor.
     * @param Config $config
     */
    public function __construct()
    {
        $this->config = new Config();
    }

    /**
     * @param $componentName
     * @param $componentTemplate
     * @param null $paramsPart
     * @param array $params
     * @param null $parentComponent
     * @param array $arFunctionParams
     * @return mixed
     */
    public function inc(
        $componentName,
        $componentTemplate,
        $paramsPart = null,
        $params = [],
        $parentComponent = null,
        $arFunctionParams = []
    ){
        global $APPLICATION;

        if ($paramsPart){
            $paramsFromFile = $this->getParamsFromFile($paramsPart);
            $params += $paramsFromFile;
        }

        return $APPLICATION->IncludeComponent(
            $componentName,
            $componentTemplate,
            $params,
            $parentComponent,
            $arFunctionParams
        );
    }

    /**
     * @param $part
     * @return array|bool|mixed
     */
    protected function getParamsFromFile($part){
        if (!$part)
            return false;

        $delimiter = $this->config->get()['settings']['delimiter'];
        $rootFolder = $this->config->getComponentPartFolder();
        $baseParamsFile = Application::getDocumentRoot() . $rootFolder . 'base.php';

        $params = $this->getFromFile($baseParamsFile);

        $ar = explode($delimiter, $part);
        $pathComp = '';

        foreach($ar as $index => $val){

            $pathComp = !$index ? $val : implode($delimiter, [$pathComp, $val]);

            $path = Application::getDocumentRoot() . $rootFolder . $pathComp . '.php';

            $params = $this->getFromFile($path) + $params;
        }

        return $params;
    }

    /**
     * @param $file
     * @return array|mixed
     */
    protected function getFromFile($file){
        if (file_exists($file)){
            return include($file);
        }

        return [];
    }
}
