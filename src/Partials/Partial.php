<?php
namespace Shantilab\BxTools\Partials;

use Bitrix\Main\Application,
    Shantilab\BxTools\Config;

/**
 * Class Partial
 * @package Shantilab\BxTools\Partials
 */
class Partial
{
    /**
     * @var Config
     */
    protected $config;
    /**
     * @var
     */
    protected $type = 'php';

    /**
     * Partial constructor.
     * @param string $type
     */
    public function __construct($type = '')
    {
        if ($type){
            $this->type = $type;
        }

        $this->config = new Config();
    }

    /**
     * @param $part
     * @param array $vars
     * @return mixed
     */
    public function inc($part, $vars = []){
        global $APPLICATION, $USER;
        $rootFolder = $this->config->getPartialsFolder();

        if ($vars)
            extract($vars, EXTR_OVERWRITE);

        $path = Application::getDocumentRoot() . $rootFolder . str_replace('.', '/', $part ). '.' . $this->type;
        return include($path);
    }
}