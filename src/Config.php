<?php
namespace Shantilab\BxTools;

use Bitrix\Main\IO\FileNotFoundException,
    Bitrix\Main\Application,
    Symfony\Component\Yaml\Yaml;

/**
 * Class Config
 * @package Shantilab\BxTools
 */
class Config
{
    /**
     * @var array
     */
    protected $bxToolsConfig;
    /**
     * @var string
     */
    protected $settingsFilePath;

    /**
     * Config constructor.
     * @param array $params
     */
    public function __construct()
    {
        $this->bxToolsConfig = (new BxToolsConfig())->get();
        $this->settingsFilePath = Application::getDocumentRoot() . $this->bxToolsConfig['settings']['path'];

        if ($this->settingsFilePath && !file_exists($this->settingsFilePath)){
            throw new FileNotFoundException($this->settingsFilePath);
        }
    }

    /**
     * @param null $val
     * @return array|null
     * @throws \InvalidArgumentException
     */
    public function get($val = null)
    {
        $params = Yaml::parse(file_get_contents($this->settingsFilePath));

        if ($val){
            $ar = explode($this->bxToolsConfig['settings']['delimiter'], $val);
            foreach($ar as $value){
                $params = $params[$value];
            }
        }

        return $params;
    }

    /**
     * @param $name
     * @return array|null|void
     */
    public function getIblockId($name)
    {
        return $this->get($this->bxToolsConfig['settings']['iblocks'] . '.' . $name);
    }

    /**
     * @return array|null
     */
    public function getComponentPartFolder()
    {
        return $this->get($this->bxToolsConfig['partials']['component_parameters']);
    }

    /**
     * @return array|null
     */
    public function getPartialsFolder()
    {
        return $this->get($this->bxToolsConfig['partials']['path']);
    }
}